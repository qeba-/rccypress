# Cypress Test

## Install Dependencies

```bash
npm install
```

## Prepare system environments

Rename `cypress.env-example.json` to `cypress.env.json`, and replace the config accordingly.

```json
{
  "RUNCLOUD_TEST_EMAIL": "email@runcloud.io",
  "RUNCLOUD_TEST_PASSWORD": "xxxxx",
  "RUNCLOUD_URL": "manage.runcloud.io",
  "TESTINDOMAIN": "sub.domain.com"
}
```

## Run Cypress

```bash
npm run start
```
