describe('Elementor Testing', () => {
    //login value change on cypress.env.json
    const username = Cypress.env('RUNCLOUD_TEST_EMAIL')
    const password = Cypress.env('RUNCLOUD_TEST_PASSWORD')
    const loginUrl = Cypress.env('RUNCLOUD_URL')
    const sitename = 'Elementor'

    it('Create WP inside RunCloud', () => {
         
            cy.visit('https://'+loginUrl+'/auth/login')
            cy.get('input[name=email]').type(username)
            cy.get('input[name=password]').type(password)
            cy.wait(2000)
            cy.get('form').submit()
            cy.server().route("POST", '**/auth/login').as('login')
            cy.wait('@login')

            //cy.wait(5000)
            cy.log('Login Now...')
            cy.server().route("POST", '**/broadcasting/auth').as('afterlogin')
            cy.wait('@afterlogin')
            cy.get('.d-flex > .font-weight-bold').click()
            cy.wait(1000)
            cy.get(':nth-child(3) > .null > span').click()
            cy.wait(2000)

             //install wordpress
             cy.log('Create WordPress')
             cy.wait(3000)
             cy.get('.panel-heading > .pull-right > .btn').click()
             cy.wait(1000)
             cy.get(':nth-child(2) > a > .btn').click()
             cy.wait(1000)
             cy.get('input[name=name]').type(sitename)
             cy.get('#runcloudDomain').click()
             cy.get('input[name=siteTitle]').type('testing123')
             cy.get('input[name=adminUsername]').type('admin')
             cy.get('input[name=password]').type('abcde123456')
             cy.get('button').contains('Add Web Application').click()
             cy.wait(3000)

            //cy.visit('https://manage.runcloud.io/servers/88270/webapplications/319477/summary?scroll=0')
            //cy.get('#sidemenu > .nav > li > .active > span').click()
            //cy.get('.table > tbody > tr > td:nth-child(1) > a').click()
             
             cy.get('li:nth-child(7) > .null > span').click();  //masuk domain name
             cy.wait(2000) 

             cy.get('.table > tbody > tr > td:nth-child(1) > a').then(($a) => {
               const wpadd = $a.text()
               cy.writeFile('cypress/fixtures/elementor.json', '{"URL": "'+ wpadd+'"}')
               cy.wait(2000)

            })

        })

        it('Install Plugins', () => {
            cy.clearCookies()
            cy.wait(3000)
            cy.log('Wait WordPress Setup...')
            cy.wait(3000)
            cy.fixture('elementor').then((elementor)  => {
                var wprul = elementor.URL
                cy.visit('http://'+wprul)
                cy.get('a[href*="wp-login.php"]').click({force: true})
                cy.wait(2000)
                cy.get('input[name=log]').type('admin')
                cy.get('input[name=pwd]').type('abcde123456')
                cy.get('form').submit()
                cy.wait(1000)
                
                cy.log('Add Plugins start here.. Install elementor')
                cy.get('#menu-plugins li:nth-child(3) > a').click({force: true})
                cy.get('.wp-filter-search').click()
                cy.get('.wp-filter-search').type('Elementor')

                cy.get('.plugin-card-elementor .install-now').click({force: true})
                cy.wait(5000) //waiting woocom
                cy.get('#menu-plugins > .wp-submenu > li.wp-first-item > .wp-first-item').click();
                cy.get('[data-slug="elementor"] > .plugin-title > .row-actions > .activate > .edit').click({force: true})
                // cy.get('.activate-now').click({force: true})
                cy.wait(2000)
                cy.get('.button-primary').click();

                //cy.visit('http://app-feilkeanu.mzunfmvbge-rz83yvqrp3d7.production-example.runcloud.site/wp-admin/edit.php?post_type=page')
                cy.get('.page-title-action').click();
                cy.get('#elementor-switch-mode-button').click();
               
                //add pages kt sini satu2. 


            })

        })

})


// Create WP at RunCloud 
// Install Elementor
// Create Page add element, save
// Create Staging Wordpress
// Add More Page and elements,
// Show Pages
// Merge Staging to Production