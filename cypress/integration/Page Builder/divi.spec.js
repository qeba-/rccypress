describe('Divi Testing', () => {
    //login value change on cypress.env.json
    const username = Cypress.env('RUNCLOUD_TEST_EMAIL')
    const password = Cypress.env('RUNCLOUD_TEST_PASSWORD')
    const loginUrl = Cypress.env('RUNCLOUD_URL')
    const sitename = 'Divi'

    it('Create WP inside RunCloud', () => {
         
            cy.visit('https://'+loginUrl+'/auth/login')
            cy.get('input[name=email]').type(username)
            cy.get('input[name=password]').type(password)
            cy.wait(2000)
            cy.get('form').submit()
            cy.server().route("POST", '**/auth/login').as('login')
            cy.wait('@login')

            //cy.wait(5000)
            cy.log('Login Now...')
            cy.server().route("POST", '**/broadcasting/auth').as('afterlogin')
            cy.wait('@afterlogin')
            cy.get('.d-flex > .font-weight-bold').click()
            cy.wait(1000)
            cy.get(':nth-child(3) > .null > span').click()
            cy.wait(2000)

             //install wordpress
             cy.log('Create WordPress')
             cy.wait(3000)
             cy.get('.panel-heading > .pull-right > .btn').click()
             cy.wait(1000)
             cy.get(':nth-child(2) > a > .btn').click()
             cy.wait(1000)
             cy.get('input[name=name]').type(sitename)
             cy.get('#runcloudDomain').click()
             cy.get('input[name=siteTitle]').type('testing123')
             cy.get('input[name=adminUsername]').type('admin')
             cy.get('input[name=password]').type('abcde123456')
             cy.get('button').contains('Add Web Application').click()
             cy.wait(3000)

            //cy.visit('https://manage.runcloud.io/servers/88270/webapplications/319477/summary?scroll=0')
            //cy.get('#sidemenu > .nav > li > .active > span').click()
            //cy.get('.table > tbody > tr > td:nth-child(1) > a').click()
             
             cy.get('li:nth-child(7) > .null > span').click();  //masuk domain name
             cy.wait(2000) 

             cy.get('.table > tbody > tr > td:nth-child(1) > a').then(($a) => {
               const wpadd = $a.text()
               cy.writeFile('cypress/fixtures/elementor.json', '{"URL": "'+ wpadd+'"}')
               cy.wait(2000)

            })

    })
})