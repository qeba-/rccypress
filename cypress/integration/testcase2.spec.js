describe('Test Case 2', () => {
    //login value change on cypress.env.json
    const username = Cypress.env('RUNCLOUD_TEST_EMAIL')
    const password = Cypress.env('RUNCLOUD_TEST_PASSWORD')
    const loginUrl = Cypress.env('RUNCLOUD_URL')


    it('Create WP inside RunCloud', () => {
         
            cy.visit('https://'+loginUrl+'/auth/login')
            cy.get('input[name=email]').type(username)
            cy.get('input[name=password]').type(password)
            cy.wait(2000)
            cy.get('form').submit()
            cy.server().route("POST", '**/auth/login').as('login')
            cy.wait('@login')
            //cy.wait(5000)
            cy.log('Login Now...')
            cy.server().route("POST", '**/broadcasting/auth').as('afterlogin')
            cy.wait('@afterlogin')
            cy.get('.d-flex > .font-weight-bold').click()
            cy.wait(1000)
            cy.get(':nth-child(3) > .null > span').click()
            cy.wait(2000)

             //install wordpress
             cy.log('Create WordPress')
             cy.wait(3000)
             cy.get('.panel-heading > .pull-right > .btn').click()
             cy.wait(1000)
             cy.get(':nth-child(2) > a > .btn').click()
             cy.wait(1000)
             cy.get('input[name=name]').type(cy.faker.name.firstName())
             cy.get('#runcloudDomain').click()
             cy.get('input[name=siteTitle]').type('testing123')
             cy.get('input[name=adminUsername]').type('admin')
             cy.get('input[name=password]').type('abcde123456')
             cy.get('button').contains('Add Web Application').click()
             cy.wait(3000)
            //cy.visit('https://manage.runcloud.io/servers/88270/webapplications/319477/summary?scroll=0')
            //cy.get('#sidemenu > .nav > li > .active > span').click()
            //cy.get('.table > tbody > tr > td:nth-child(1) > a').click()
             
             cy.get('li:nth-child(7) > .null > span').click();  //masuk domain name
             cy.wait(2000) 

             cy.get('.table > tbody > tr > td:nth-child(1) > a').then(($a) => {
               const wpadd = $a.text()
               cy.writeFile('cypress/fixtures/stgcase2.json', '{"URL": "'+ wpadd+'"}')
               cy.wait(2000)
           })

        })

        it('Install WooCommerce + Add Product (Production Site)', () => {
            cy.clearCookies()
            cy.wait(3000)
            cy.log('Wait WordPress Setup...')
            cy.fixture('stgcase2').then((stgcase2)  => {
                var wprul = stgcase2.URL
                cy.visit('http://'+wprul)
                cy.get('a[href*="wp-login.php"]').click({force: true})
                cy.wait(2000)
                cy.get('input[name=log]').type('admin')
                cy.get('input[name=pwd]').type('abcde123456')
                cy.get('form').submit()
                cy.wait(1000)
                
                cy.log('Add Plugins start here..')
                cy.get('#menu-plugins li:nth-child(3) > a').click({force: true})
                cy.get('.wp-filter-search').click()
                cy.get('.wp-filter-search').type('WooCommerce')

              
                cy.get('.plugin-card-woocommerce .install-now').click({force: true})
                cy.wait(17000) //waiting woocom
                cy.get('#menu-plugins > .wp-submenu > li.wp-first-item > .wp-first-item').click();
                cy.get('[data-slug="woocommerce"] > .plugin-title > .row-actions > .activate > .edit').click({force: true})
               // cy.get('.activate-now').click({force: true})
                cy.wait(2000)
               
                cy.log('Start setup WooCommerce')
                cy.wait(2000)
                cy.visit('http://'+wprul+'/wp-admin/admin.php?page=wc-setup&step=store_setup') //direct access link
            
                //cy.get('.wc-setup-footer-links').click({force: true}) //enable of disable depends on setup. 
                cy.get('#select2-store_country-container').click();
                cy.get('.select2-search__field').click();
                cy.get('.select2-search__field').type('malaysia{enter}');

                cy.get('#store_address').click();
                cy.get('#store_address').type('SP5 The Street Mall Persiaran Multimedia');
                cy.get('#store_city').click();
                cy.get('#store_city').type('Cyberjaya');
                cy.get('#store_postcode').click();
                cy.get('#store_postcode').type('63000');
                cy.get('#select2-store_state-container').click();
                cy.get('.select2-search__field').type('selangor{enter}');
                cy.get('#select2-currency_code-container').click();
                cy.get('.select2-search__field').click();
                cy.get('.select2-search__field').type('myr{enter}');
                cy.get('.button-primary').click();
                cy.get('.address-step').submit();
                cy.get('#wc_tracker_submit').click({force: true})
                cy.wait(2000)
              
                
                cy.get('.wc-wizard-payment-gateway-form > .in-cart > .wc-wizard-service-item > .wc-wizard-service-enable > .wc-wizard-service-toggle').click()
                cy.get('.wc-setup > .wc-setup-content > .wc-wizard-payment-gateway-form > .wc-setup-actions > .button-primary').click()
                cy.get('.button-primary').click();
                cy.wait(2000)
                cy.get('form > .wc-wizard-services > .wc-wizard-service-item:nth-child(2) > .wc-wizard-service-enable > .wc-wizard-service-toggle').click()
                cy.get('form > .wc-wizard-services > .wc-wizard-service-item:nth-child(3) > .wc-wizard-service-enable > .wc-wizard-service-toggle').click()
                cy.get('.button-primary').click();
                //cy.get('form').submit();
                cy.wait(2000)
                cy.get('.wc-setup-footer-links').click()
                cy.get('.wc-setup-footer-links').click()
                cy.get(':nth-child(1) > .wc-wizard-next-step-action > .wc-setup-actions > .button').click();
                cy.log('Done WoocCom setup....')
                cy.wait(3000)
                cy.visit('http://'+wprul+'/wp-admin/post-new.php?post_type=product')
                //cy.get('#menu-posts-product li:nth-child(3) > a').click({force: true}) //guna masa testing nk cepat
                //cy.get('.close').click({force: true})
                cy.wait(3000)
                cy.get('#title').type('test product 1')
                cy.get('#\_regular_price').click()
                cy.get('#\_regular_price').type('10')
                cy.get('#\_sale_price').click()
                cy.get('.inventory_options > a').click()
                cy.get('.shipping_options > a').click()
                cy.get('#\_weight').click()
                cy.get('#\_weight').type('10')
                cy.get('#publish').click()
                cy.get('#post').submit()
                cy.wait(2000)
                cy.get('#menu-posts-product > .wp-submenu > :nth-child(3) > a').click()
                
                cy.get('#title').type('test product 2')
                cy.get('#\_regular_price').click()
                cy.get('#\_regular_price').type('20')
                cy.get('#\_sale_price').click()
                cy.get('.inventory_options > a').click()
                cy.get('.shipping_options > a').click()
                cy.get('#\_weight').click()
                cy.get('#\_weight').type('20')
                cy.get('#publish').click()
                cy.get('#post').submit()
                cy.wait(2000)
                cy.get('#menu-posts-product > .wp-submenu > :nth-child(3) > a').click()
                
                cy.get('#title').type('test product 3')
                cy.get('#\_regular_price').click()
                cy.get('#\_regular_price').type('20')
                cy.get('#\_sale_price').click()
                cy.get('.inventory_options > a').click()
                cy.get('.shipping_options > a').click()
                cy.get('#\_weight').click()
                cy.get('#\_weight').type('20')
                cy.get('#publish').click()
                cy.get('#post').submit()
                cy.log('Finish Add Product')
                cy.wait(2000)

            })


        })
  
        it('Create Staging Wordpress at RunCloud', () => {
            
            cy.visit('https://'+loginUrl+'/auth/login')
            cy.get('input[name=email]').type(username)
            cy.get('input[name=password]').type(password)
            cy.wait(2000)
            cy.get('form').submit()
            cy.server().route("POST", '**/auth/login').as('login')
            cy.wait('@login')
            cy.wait(1000)
            //cy.wait(5000)
            cy.log('Login Now...')
            cy.wait(5000)
            cy.get('.d-flex > .font-weight-bold').click()
            cy.get(':nth-child(3) > .null > span').click()
            cy.wait(2000)
            cy.get('tr > :nth-child(1) > a').click()
            cy.get(':nth-child(15) > .null > span').click()
            cy.wait(1000)
            cy.get('.panel > .panel-body > form > .form-group:nth-child(1) > .form-control').click()
            cy.get('.panel > .panel-body > form > .form-group:nth-child(1) > .form-control').click()
            cy.get('.panel > .panel-body > form > .form-group:nth-child(1) > .form-control').type(cy.faker.name.firstName() +'-stg.dev1.hadiantech.com')
            cy.get('.panel > .panel-body > form > .form-group:nth-child(5) > .form-control').click()
            cy.get('.panel > .panel-body > form > .form-group:nth-child(5) > .form-control').type('admin')
            cy.get('.panel-body > form > .form-group > .input-group > .form-control').type('admin123')
            cy.get('.panel > .panel-body > form > .form-group:nth-child(7) > .form-control').type('admin123')
            cy.get('[type="submit"]').click()
            cy.wait(7000)
            cy.get('.panel-body > :nth-child(1) > :nth-child(1) > p').then(($p) => {
                const stagcs2 = $p.text()
                cy.writeFile('cypress/fixtures/stg2.json', '{"URL": "'+stagcs2+'"}')
                cy.wait(2000) 
                cy.log('Complete at RC go to WP Stagging')
                cy.wait(2000)
            })
            cy.wait(2000)
        })

        it('Add more product at Staging', function() {
            cy.wait(2000)
            cy.fixture('stg2').then((stg2)  => {
                var wprul = stg2.URL
                cy.visit('http://admin:admin123@'+wprul)
                cy.get('a[href*="wp-login.php"]').click({force: true})
                cy.wait(2000) 
                cy.get('input[name=log]').type('admin')
                cy.get('input[name=pwd]').type('abcde123456')
                cy.get('form').submit()
                cy.wait(1000) 

                cy.log('Add new product on Stagging...')
                cy.get('#menu-posts-product li:nth-child(3) > a').click({force: true}) //guna masa testing nk cepat
                cy.get('.close').click({force: true})
                cy.get('.wc-setup-actions:nth-child(1) > .button-primary').click();
                cy.get('#title').type('test product 4')
                cy.get('#content_ifr')
                cy.get('#\_regular_price').click()
                cy.get('#\_regular_price').type('40')
                cy.get('#\_sale_price').click()
                cy.get('.inventory_options > a').click()
                cy.get('.shipping_options > a').click()
                cy.get('#\_weight').click()
                cy.get('#\_weight').type('40')
                cy.get('#publish').click()
                cy.get('#post').submit()
                cy.wait(2000)
                cy.get('#menu-posts-product > .wp-submenu > :nth-child(3) > a').click()

                cy.log('Add new product on Stagging...')
                cy.get('#menu-posts-product li:nth-child(3) > a').click({force: true}) //guna masa testing nk cepat
                cy.get('.close').click({force: true})
                cy.get('.wc-setup-actions:nth-child(1) > .button-primary').click();
                cy.get('#title').type('test product 5')
                cy.get('#content_ifr')
                cy.get('#\_regular_price').click()
                cy.get('#\_regular_price').type('50')
                cy.get('#\_sale_price').click()
                cy.get('.inventory_options > a').click()
                cy.get('.shipping_options > a').click()
                cy.get('#\_weight').click()
                cy.get('#\_weight').type('50')
                cy.get('#publish').click()
                cy.get('#post').submit()
                cy.wait(2000)


            })
            
        })

        it('Merge Staging to Production at RunCloud', function() {
            //cannot continue stuck at Staging.
            
        })



})


// Create WP at RunCloud 
// Install WooCommerce
// Basic setup WooCommerce
// Add Product
// Create Staging Wordpress
// Add More Product at Staging
// Merge Staging to Production