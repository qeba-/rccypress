describe('Hummingbird - Speed Optimize, Cache,', function() {
    //login value change on cypress.env.json
    const username = Cypress.env('RUNCLOUD_TEST_EMAIL')  
    const password = Cypress.env('RUNCLOUD_TEST_PASSWORD')  
    const loginUrl = Cypress.env('RUNCLOUD_URL')
    const testdomain = Cypress.env('TESTINDOMAIN') //this domain should point to correct IP
    const testname = 'hummingbird'


    it('Login RunCloud craete WordPress', () => {
        cy.visit('https://'+loginUrl+'/auth/login')
        cy.get('input[name=email]').type(username)
        cy.get('input[name=password]').type(password)
        cy.wait(2000)
        cy.get('form').submit()
        cy.server().route("POST", '**/auth/login').as('login')
        cy.wait('@login')

        //cy.wait(5000)
        cy.log('Login Now...')
        cy.server().route("POST", '**/broadcasting/auth').as('afterlogin')
        cy.wait('@afterlogin')
        cy.get('.d-flex > .font-weight-bold').click()
        cy.wait(1000)
        cy.get(':nth-child(3) > .null > span').click()
        cy.wait(2000)

        //install wordpress
        cy.log('Create WordPress')
        cy.wait(3000)
        cy.get('.panel-heading > .pull-right > .btn').click()
        cy.wait(1000)
        cy.get(':nth-child(2) > a > .btn').click()
        cy.wait(1000)
        cy.get('input[name=name]').type(testname)
        
        cy.get(".panel").then(panel => {
            const input = panel.find("[name=domainName]");
            if (panel.find("#runcloudDomain").length == 0) {
              cy.get('form > :nth-child(2) > .form-control').type(testname+'.'+testdomain)
              //return input.val("site.example.com")
            }
          
            panel.find("#runcloudDomain").click();
            input.val(testname);
  
          });

        cy.get('input[name=siteTitle]').type(testname)
        cy.get('input[name=adminUsername]').type('admin')
        cy.get('input[name=password]').type('abcde123456')
        cy.get('button').contains('Add Web Application').click()
        cy.wait(3000)

        //create htaccess file using file manager
        cy.get('li:nth-child(6) span').click();
        cy.wait(5000)
        cy.get('.btn-success').click();
        cy.get('.open li:nth-child(1) > a').click({force: true})
        cy.get('.swal2-input').type('.htaccess');
        cy.get('.swal2-confirm').click({force: true})
        cy.wait(1000)
        
        cy.get('li:nth-child(7) > .null > span').click();  //masuk domain name
        cy.wait(2000) 

        cy.get('.table > tbody > tr > td:nth-child(1) > a').then(($a) => {
          const wpadd = $a.text()
          cy.writeFile('cypress/fixtures/Hummingbird.json', '{"URL": "'+ wpadd+'"}')
          cy.wait(2000)
        
        })
    })


    it('Login WordPress and Install Hummingbird Plugins', () => {
        cy.wait(1000) 
        cy.fixture('Hummingbird').then((Hummingbird)  => {
            var wprul = Hummingbird.URL
            cy.visit('http://'+wprul)
            cy.get('a[href*="wp-login.php"]').click({force: true})
            cy.wait(2000) 
            cy.get('input[name=log]').type('admin')
            cy.get('input[name=pwd]').type('abcde123456')
            cy.get('form').submit()
            cy.wait(1000) 

            //set permalink
            cy.log('Setting WP permalink...')
            cy.visit('http://'+wprul+'/wp-admin/options-permalink.php')
            cy.get('tr:nth-child(5) input').click();
            cy.get('#submit').click();
            cy.wait(2000) 
 
            cy.visit('http://'+wprul+'/wp-admin/')

            cy.get('#menu-plugins li:nth-child(3) > a').click({force: true})
            cy.get('.wp-filter-search').click()
            cy.get('.wp-filter-search').type('Hummingbird')
            cy.wait(3000)
            cy.get('.plugin-card-hummingbird-performance .install-now').click({force: true})
            cy.wait(10000)
            cy.get('.activate-now').click({force: true})
            cy.wait(1000)
           // cy.get('#toplevel_page_wpcf7 li:nth-child(3) > a').click({force: true});
           

            cy.wait(1000)
            //enable cache
            cy.log('Setup Chache Plugins')
            cy.visit('http://'+wprul+'/wp-admin/admin.php?page=wphb-caching') 
            cy.get('#skip-quick-setup').click({force: true})
            cy.wait(1000)
            cy.get('#activate-page-caching').click();


            cy.wait(2000)
            //add new pages
            cy.get('#menu-pages li:nth-child(3) > a').click({force: true})
            cy.get('div > .editor-post-title > .wp-block > div > #post-title-0').click({force: true})
            cy.get('#post-title-0').type('prod page', {force: true})
            cy.get('div > .editor-post-title > .wp-block > div > #post-title-0').click({force: true})
            cy.get('#post-title-0').type('test image production', {force: true})
            cy.get('.editor-default-block-appender__content').click({force: true})
            cy.get('.rich-text').click()
            cy.get('.components-button:nth-child(1) > .editor-block-icon > svg').click()
            cy.get('.editor-media-placeholder__url-input-container > .components-button').click()
            cy.get('.editor-media-placeholder__url-input-field').type('https://cf1.cdn.runcloud.io/img/hero/hero-banner.svg');
            cy.get('.dashicons-editor-break > path').click({force: true})
            cy.get('.editor-default-block-appender__content').click({force: true})
            cy.get('.editor-post-publish-panel__toggle').click()
            cy.get('.editor-post-publish-button').click().click({force: true});
            cy.wait(5000)

            cy.log('View pages created...')
            cy.get('.post-publish-panel__postpublish-buttons > .components-button').click({force: true});
            cy.log('Wait for cache and page load....')
            cy.wait(7000)
        })
    })


        it('Login to RunCloud to Create Stagging', () => {
            cy.visit('https://'+loginUrl+'/auth/login')
            cy.get('input[name=email]').type(username)
            cy.get('input[name=password]').type(password)
            //cy.wait(3000)
            cy.get('form').submit()
            cy.server().route("POST", '**/auth/login').as('login')
            cy.wait('@login')
            cy.wait(3000)

            //stagging
            cy.log('Create Stagging')
            cy.get('.title').click();
            cy.get('.nav-stacked > li:nth-child(3) > .null').click();
            cy.wait(3000)
            cy.get('tr > :nth-child(1) > a').click({force: true})
            cy.get(':nth-child(15) > .null > span').click()
            cy.wait(1000)
            
            cy.get('form > :nth-child(2) > .form-control').click()
            cy.get('form > :nth-child(2) > .form-control').type(testname+'-stg.'+testdomain)
            cy.get('form > :nth-child(6) > .form-control').click()
            cy.get('form > :nth-child(6) > .form-control').type('admin')
            cy.get(':nth-child(7) > .input-group > .form-control').click()
            cy.get(':nth-child(7) > .input-group > .form-control').type('admin123')
            cy.get(':nth-child(8) > .form-control').click()
            cy.get(':nth-child(8) > .form-control').type('admin123')

            cy.get('[type="submit"]').click()
            cy.wait(7000)
            cy.get('.panel-body > :nth-child(1) > :nth-child(1) > p').then(($p) => {
            const stagcs4 = $p.text()
            cy.writeFile('cypress/fixtures/stghmd.json', '{"URL": "'+stagcs4+'"}')
            cy.wait(2000) 
            cy.log('Complete at RC go to WP Stagging')
            })

        }) 


        it('Login to staging WordPress, Create more post + Image', () => {
            cy.wait(1000) 
            cy.fixture('stghmd').then((stghmd)  => {
                var wprul = stghmd.URL
                cy.visit('http://admin:admin123@'+wprul)
                cy.get('a[href*="wp-login.php"]').click({force: true})
                cy.wait(2000) 
                cy.get('input[name=log]').type('admin')
                cy.get('input[name=pwd]').type('abcde123456')
                cy.get('form').submit()
                cy.wait(1000) 
                
               // create 1 page
                cy.get('#menu-pages li:nth-child(3) > a').click({force: true})
                cy.wait(1000)
                cy.get('div > .editor-post-title > .wp-block > div > #post-title-0').click({force: true})
                cy.get('#post-title-0').type('test image 1')
                cy.get('.editor-default-block-appender__content').click({force: true})
                cy.get('.rich-text').click()
                cy.get('.components-button:nth-child(1) > .editor-block-icon > svg').click()
                cy.get('.editor-media-placeholder__url-input-container > .components-button').click()
                cy.get('.editor-media-placeholder__url-input-field').type('https://cf1.cdn.runcloud.io/img/hero/hero-banner.svg');
                cy.get('.dashicons-editor-break > path').click({force: true})
                cy.get('.editor-default-block-appender__content').click({force: true})
                cy.get('.editor-post-publish-panel__toggle').click()
                cy.get('.editor-post-publish-button').click().click({force: true});
                cy.wait(5000)
                cy.log('View pages created 1...')
                cy.get('.post-publish-panel__postpublish-buttons > .components-button').click({force: true});
                cy.wait(7000)

                //create 2 page
                cy.visit('http://admin:admin123@'+wprul+'/wp-admin/index.php')
                cy.get('#menu-pages li:nth-child(3) > a').click({force: true})
                cy.wait(1000)
                cy.get('div > .editor-post-title > .wp-block > div > #post-title-0').click({force: true})
                cy.get('#post-title-0').type('test image 2')
                cy.get('.editor-default-block-appender__content').click({force: true})
                cy.get('.rich-text').click()
                cy.get('.components-button:nth-child(1) > .editor-block-icon > svg').click()
                cy.get('.components-form-file-upload > .components-button')
                cy.get('.editor-writing-flow__click-redirect').click();
                cy.get('.block-editor-media-placeholder__upload-button').click({force: true});
                cy.get('.editor-media-placeholder__url-input-container > .components-button').click()
                cy.get('.editor-media-placeholder__url-input-field').type('https://blog.runcloud.io/wp-content/uploads/2020/03/runcloud-server-provisioning-release3-720x370.png');
                cy.get('.dashicons-editor-break > path').click({force: true})
                cy.wait(1000)
                cy.get('.editor-post-publish-panel__toggle').click()
                cy.get('.editor-post-publish-button').click().click({force: true});
                cy.wait(4000)
                cy.log('View pages created 1...')
                cy.get('.post-publish-panel__postpublish-buttons > .components-button').click({force: true});
                cy.wait(7000)
                cy.log('Complete create page + image')
                cy.wait(3000)
            
            })
        })

        it('View pages at Staging', () => {

            cy.fixture('stghmd').then((stghmd)  => {
                var wprul = stghmd.URL
                cy.visit('http://admin:admin123@'+wprul)
                cy.wait(3000) 
                cy.log('View few pages only...')
                cy.wait(5000) 
                cy.get('.header-navigation-wrapper > .primary-menu-wrapper > .primary-menu > .page-item-2 > a').click()
                cy.wait(5000)
                cy.get('.header-navigation-wrapper > .primary-menu-wrapper > .primary-menu > .page-item-5 > a').click()
                cy.wait(5000)
            })
        })

        it('View pages at Production', () => {
    
            cy.fixture('Hummingbird').then((Hummingbird)  => {
                var wprul = Hummingbird.URL
                cy.visit('http://'+wprul)
                cy.wait(3000) 
                cy.log('View few pages only...')
                cy.wait(5000) 
                cy.get('.header-navigation-wrapper > .primary-menu-wrapper > .primary-menu > .page-item-2 > a').click()
                cy.wait(5000)
                cy.get('.header-navigation-wrapper > .primary-menu-wrapper > .primary-menu > .page-item-5 > a').click()
                cy.wait(5000)
            })
        })

        it('Merge Staging to Production + Show Staging Page', () => {
                cy.wait(3000)
                cy.visit('https://'+loginUrl+'/auth/login')
                cy.get('input[name=email]').type(username)
                cy.get('input[name=password]').type(password)
                cy.wait(3000)
                cy.get('form').submit()
                cy.server().route("POST", '**/auth/login').as('login')
                cy.wait('@login')
                
                //merge
                cy.log('Merge Stagging to Production')
                cy.wait(2000)
                cy.get('.d-flex > .font-weight-bold').click();
                cy.get('.nav-stacked > li:nth-child(3) > .null').click();
                cy.wait(1000)
                //cy.get('tr:nth-child(1) > td:nth-child(1) > a').click();
                cy.get('tbody > :nth-child(1) > :nth-child(1) > a').click();
                cy.get('li:nth-child(15) span').click();
                cy.get('tbody > :nth-child(1) > :nth-child(1) > a').click();
                cy.wait(3000)
                cy.get(':nth-child(15) > .null > span').click()
                //cy.get('#sidemenu > .nav > li:nth-child(15) > .null > span').click() //cara lama
                cy.wait(1000)
                cy.get('.item:nth-child(2) .btn').click();
                cy.get('.btn-base').click();
                cy.get('form').submit();
                cy.get('.swal2-buttonswrapper').click();
                cy.get('.swal2-confirm').click();
                cy.wait(3500)

                // open merged URL 
                cy.log('open merged URL')
                cy.get('#sidemenu > .nav > li:nth-child(7) > .null > span').click()
                cy.get(':nth-child(1) > p > a').click()
                //cy.get('.table > tbody > tr > td:nth-child(1) > a').click() //cara lama
        
                cy.log('Complete! Testing')

     })


})
