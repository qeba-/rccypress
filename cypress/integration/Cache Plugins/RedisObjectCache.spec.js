describe('Redis Object Cache Test', function() {
    //login value change on cypress.env.json
    const username = Cypress.env('RUNCLOUD_TEST_EMAIL')  
    const password = Cypress.env('RUNCLOUD_TEST_PASSWORD')  
    const loginUrl = Cypress.env('RUNCLOUD_URL')
    const testdomain = Cypress.env('TESTINDOMAIN') //this domain should point to correct IP
    const testname = 'RedisObjectCache'


    it('Login RunCloud craete WordPress', () => {

        cy.visit('https://'+loginUrl+'/auth/login')
        cy.get('input[name=email]').type(username)
        cy.get('input[name=password]').type(password)
        cy.wait(2000)
        cy.get('form').submit()
        cy.server().route("POST", '**/auth/login').as('login')
        cy.wait('@login')

        //cy.wait(5000)
        cy.log('Login Now...')
        cy.server().route("POST", '**/broadcasting/auth').as('afterlogin')
        cy.wait('@afterlogin')
        cy.get('.d-flex > .font-weight-bold').click()
        cy.wait(1000)
        cy.get(':nth-child(3) > .null > span').click()
        cy.wait(2000)

        //install wordpress
        cy.log('Create WordPress')
        cy.wait(3000)
        cy.get('.panel-heading > .pull-right > .btn').click()
        cy.wait(1000)
        cy.get(':nth-child(2) > a > .btn').click()
        cy.wait(1000)
        cy.get('input[name=name]').type(testname)

        cy.get(".panel").then(panel => {
            const input = panel.find("[name=domainName]");
            if (panel.find("#runcloudDomain").length == 0) {
              cy.get('form > :nth-child(2) > .form-control').type(testname+'.'+testdomain)
              //return input.val("site.example.com")
            }
          
            panel.find("#runcloudDomain").click();
            input.val(testname);
  
          });

        cy.get('input[name=siteTitle]').type(testname)
        cy.get('input[name=adminUsername]').type('admin')
        cy.get('input[name=password]').type('abcde123456')
        cy.get('button').contains('Add Web Application').click()
        cy.wait(3000)

        //create htaccess file using file manager
        cy.get('li:nth-child(6) span').click();
        cy.wait(5000)
        cy.get('.btn-success').click();
        cy.get('.open li:nth-child(1) > a').click({force: true})
        cy.get('.swal2-input').type('.htaccess');
        cy.get('.swal2-confirm').click({force: true})
        cy.wait(1000)
        
        cy.get('li:nth-child(7) > .null > span').click();  //masuk domain name
        cy.wait(2000) 

        cy.get('.table > tbody > tr > td:nth-child(1) > a').then(($a) => {
          const wpadd = $a.text()
          cy.writeFile('cypress/fixtures/redis.json', '{"URL": "'+ wpadd+'"}')
          cy.wait(2000)
        
        })
    
    })

    it('Login WordPress and Install Redis Object Cache Plugins', () => {
        cy.wait(1000) 
        cy.fixture('redis').then((redis)  => {
            var wprul = redis.URL
            cy.visit('http://'+wprul)
            cy.get('a[href*="wp-login.php"]').click({force: true})
            cy.wait(2000) 
            cy.get('input[name=log]').type('admin')
            cy.get('input[name=pwd]').type('abcde123456')
            cy.get('form').submit()
            cy.wait(1000)

            //set permalink
            cy.log('Setting WP permalink...')
            cy.visit('http://'+wprul+'/wp-admin/options-permalink.php')
            cy.get('tr:nth-child(5) input').click();
            cy.get('#submit').click();
            cy.wait(2000) 

            //begin install  plugins.. 
            cy.visit('http://'+wprul+'/wp-admin/')
            cy.get('#menu-plugins li:nth-child(3) > a').click({force: true})
            cy.get('.wp-filter-search').click()
            cy.get('.wp-filter-search').type('Redis Object Cache')
            cy.wait(3000)
            cy.get('.plugin-card-redis-cache .install-now').click({force: true})
            cy.wait(10000)
            cy.get('.activate-now').click({force: true})
            cy.wait(1000)

            //enable cache
            cy.log('Enable Cache')
            cy.visit('http://'+wprul+'/wp-admin/options-general.php?page=redis-cache') 
            cy.get('.button-large').click();
            cy.wait(3000)
           
            cy.visit('http://'+wprul+'/wp-admin') 
            cy.get('#menu-pages li:nth-child(3) > a').click({force: true})
            cy.get('.components-modal__header > .components-button').click();
            cy.get('div > .editor-post-title > .wp-block > div > #post-title-0').click({force: true})
            cy.get('#post-title-0').type('test page production', {force: true})
            cy.get('.components-dropdown:nth-child(1) path').click();
            cy.get('.editor-block-list-item-image > .block-editor-block-types-list__item-icon').click();
            cy.get('.block-editor-media-placeholder__url-input-container > .components-button').click();
            cy.get('.block-editor-media-placeholder__url-input-field').type('https://cf1.cdn.runcloud.io/img/hero/hero-banner.svg');
            cy.get('.block-editor-media-placeholder__url-input-submit-button > svg').click();

            cy.get('.editor-post-publish-panel__toggle').click()
            cy.get('.editor-post-publish-button').click().click({force: true});
            cy.wait(5000)

            //view pages created
            cy.log('View pages created...')
            cy.get('.post-publish-panel__postpublish-buttons > .components-button').click({force: true});
            cy.log('Wait for cache and page load....')
            cy.wait(7000)

        })
    })


    it('Login to RunCloud to Create Stagging', () => {
        cy.visit('https://'+loginUrl+'/auth/login')
        cy.get('input[name=email]').type(username)
        cy.get('input[name=password]').type(password)
        //cy.wait(3000)
        cy.get('form').submit()
        cy.server().route("POST", '**/auth/login').as('login')
        cy.wait('@login')
        cy.wait(3000)

        //stagging
        cy.log('Create Stagging')
        cy.get('.title').click();
        cy.get('.nav-stacked > li:nth-child(3) > .null').click();
        cy.wait(3000)
        cy.get('tr > :nth-child(1) > a').click({force: true})
        cy.get(':nth-child(15) > .null > span').click()
        cy.wait(1000)
        
        cy.get('form > :nth-child(2) > .form-control').click()
        cy.get('form > :nth-child(2) > .form-control').type(testname+'-stg.'+testdomain)
        cy.get('form > :nth-child(6) > .form-control').click()
        cy.get('form > :nth-child(6) > .form-control').type('admin')
        cy.get(':nth-child(7) > .input-group > .form-control').click()
        cy.get(':nth-child(7) > .input-group > .form-control').type('admin123')
        cy.get(':nth-child(8) > .form-control').click()
        cy.get(':nth-child(8) > .form-control').type('admin123')

        cy.get('[type="submit"]').click()
        cy.wait(7000)
        cy.get('.panel-body > :nth-child(1) > :nth-child(1) > p').then(($p) => {
            const stagcs4 = $p.text()
            cy.writeFile('cypress/fixtures/stgRedis.json', '{"URL": "'+stagcs4+'"}')
            cy.wait(2000) 
            cy.log('Complete at RC go to WP Stagging')
        })

    }) 
    
    it('View pages at Staging', () => {

        cy.fixture('stgRedis').then((stgRedis)  => {
            var wprul = stgRedis.URL
            cy.visit('http://admin:admin123@'+wprul)
            cy.wait(3000) 
            cy.log('View pages at staging.....')
            cy.wait(5000) 
            cy.get('.header-navigation-wrapper > .primary-menu-wrapper > .primary-menu > .page-item-2 > a').click()
            cy.wait(5000)
            cy.get('.header-navigation-wrapper > .primary-menu-wrapper > .primary-menu > .page-item-5 > a').click()
            cy.wait(5000)
        })
    })

    it('View pages at Production', () => {
    
        cy.fixture('redis').then((redis)  => {
            var wprul = redis.URL
            cy.visit('http://'+wprul)
            cy.wait(3000) 
            cy.log('View pages and check URL at Production.')
            cy.wait(5000) 
            cy.get('.header-navigation-wrapper > .primary-menu-wrapper > .primary-menu > .page-item-2 > a').click()
            cy.wait(5000)
            cy.get('.header-navigation-wrapper > .primary-menu-wrapper > .primary-menu > .page-item-5 > a').click()
            cy.log('Monitor URL redirect')
            cy.log('ERROR WILL APPEAR, SINCE IT REQUEST THE STAGING SITE..')
            cy.log('fINISH TEST')
            cy.wait(5000)
        })
    })


    
})