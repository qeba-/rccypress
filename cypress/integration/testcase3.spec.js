describe('Test case 3', function() {
    //login value change on cypress.env.json
    const username = Cypress.env('RUNCLOUD_TEST_EMAIL')  
    const password = Cypress.env('RUNCLOUD_TEST_PASSWORD') 
    const loginUrl = Cypress.env('RUNCLOUD_URL')
 

    it('Test Case 3 - Change URL then Merge', function() {
        cy.visit('https://'+loginUrl+'/auth/login')
        cy.get('input[name=email]').type(username)
        cy.get('input[name=password]').type(password)
        cy.get('form').submit()
        cy.wait(5000)
        cy.get('.d-flex > .font-weight-bold').click()
        cy.wait(5000)
        cy.get(':nth-child(3) > .null > span').click()
        cy.wait(1000)
        cy.get('.panel-heading > .pull-right > .btn').click()
        cy.wait(1000)
        cy.get(':nth-child(2) > a > .btn').click()
        
        //install wordpress
        cy.log('Create WordPress')
        cy.wait(3000)
        cy.get('input[name=name]').type(cy.faker.name.firstName())
        cy.get('#runcloudDomain').click()
        cy.get('input[name=siteTitle]').type('testing123')
        cy.get('input[name=adminUsername]').type('admin')
        cy.get('input[name=password]').type('abcde123456')
        cy.get('button').contains('Add Web Application').click()
        cy.wait(3000)
        

        //stagging
        cy.log('Create Stagging')
        cy.get(':nth-child(15) > .null > span').click()
        cy.wait(1000)
        cy.get('.panel > .panel-body > form > .form-group:nth-child(1) > .form-control').click()
        cy.get('.panel > .panel-body > form > .form-group:nth-child(1) > .form-control').click()
        cy.get('.panel > .panel-body > form > .form-group:nth-child(1) > .form-control').type(faker.name.firstName() +'-stg.dev1.hadiantech.com')
        cy.get('.panel > .panel-body > form > .form-group:nth-child(5) > .form-control').click()
        cy.get('.panel > .panel-body > form > .form-group:nth-child(5) > .form-control').type('admin')
        cy.get('.panel-body > form > .form-group > .input-group > .form-control').type('admin123')
        cy.get('.panel > .panel-body > form > .form-group:nth-child(7) > .form-control').type('admin123')
        cy.wait(1000)
        cy.get('.rc-webapp > .panel > .panel-body > form > .btn').click()


 
        //change production URL
        cy.log('Change URL')
        cy.get('#sidemenu > .nav > li:nth-child(7) > .null > span').click()
        cy.wait(2000)
        cy.get(':nth-child(7) > .null > span').click()
        cy.get('.panel-body > div > form > .form-group > .form-control').click()
        cy.get('input[name=name]').type(cy.faker.name.firstName() +'.dev1.hadiantech.com')
        cy.get('.panel > .panel-body > div > form > .btn').click()
        cy.get('button').contains('Attach Domain Name').click()
        cy.wait(6000)
        cy.get('.table > tbody > tr:nth-child(1) > .text-center > .text-danger').click()
        cy.get('.swal2-shown > .swal2-container > .swal2-modal > .swal2-buttonswrapper > .swal2-confirm').click()

        //merge
        cy.log('Merge Stagging to Production')
        cy.wait(2000)
        cy.get('#sidemenu > .nav > li:nth-child(15) > .null > span').click()
        cy.wait(1000)
        cy.get('.item:nth-child(2) .btn').click();
        cy.get('.btn-base').click();
        cy.get('form').submit();
        cy.get('.swal2-buttonswrapper').click();
        cy.get('.swal2-confirm').click();
        cy.wait(3500)

        // open merged URL 
        cy.log('open merged URL')
        cy.get('#sidemenu > .nav > li:nth-child(7) > .null > span').click()
        cy.get('.table > tbody > tr > td:nth-child(1) > a').click()
  
        cy.log('Complete! Testing!')
})

})