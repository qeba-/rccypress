describe('Test Case 1', function() {
    //login value change on cypress.env.json
    const username = Cypress.env('RUNCLOUD_TEST_EMAIL')
    const password = Cypress.env('RUNCLOUD_TEST_PASSWORD')
    const loginUrl = Cypress.env('RUNCLOUD_URL')

    var shrtcode = ""
    var ctf7 = ""

    it('Test Case 1 - Create WP > install Contact Form-7 > Merge to Production', () => {
      cy.visit('https://'+loginUrl+'/auth/login')
       cy.get('input[name=email]').type(username)
       cy.get('input[name=password]').type(password)
       cy.wait(3000)
       cy.get('form').submit()
       cy.wait(3000)

        cy.get('.d-flex > .font-weight-bold').click()
        cy.wait(5000)
        cy.get(':nth-child(3) > .null > span').click()
        cy.wait(1000)
        cy.get('.panel-heading > .pull-right > .btn').click()
        cy.wait(1000)
        cy.get(':nth-child(2) > a > .btn').click()

        //install wordpress
        cy.log('Create WordPress')
        cy.wait(3000)
        cy.get('input[name=name]').type(cy.faker.name.firstName())
        cy.get('#runcloudDomain').click()
        cy.get('input[name=siteTitle]').type('testing123')
        cy.get('input[name=adminUsername]').type('admin')
        cy.get('input[name=password]').type('abcde123456')
        cy.get('button').contains('Add Web Application').click()
        cy.wait(3000)

        //stagging
        cy.log('Create Stagging')
        cy.get(':nth-child(15) > .null > span').click()
        cy.wait(1000)
        cy.get('.panel > .panel-body > form > .form-group:nth-child(1) > .form-control').click()
        cy.get('.panel > .panel-body > form > .form-group:nth-child(1) > .form-control').click()
        cy.get('.panel > .panel-body > form > .form-group:nth-child(1) > .form-control').type(cy.faker.name.firstName() +'-stg.dev1.hadiantech.com')
        cy.get('.panel > .panel-body > form > .form-group:nth-child(5) > .form-control').click()
        cy.get('.panel > .panel-body > form > .form-group:nth-child(5) > .form-control').type('admin')
        cy.get('.panel-body > form > .form-group > .input-group > .form-control').type('admin123')
        cy.get('.panel > .panel-body > form > .form-group:nth-child(7) > .form-control').type('admin123')
        cy.get('[type="submit"]').click()
        cy.wait(7000)
        cy.get('.panel-body > :nth-child(1) > :nth-child(1) > p').then(($p) => {
        const wpadd = $p.text()
        cy.writeFile('cypress/fixtures/stgcase1.json', '{"URL": "'+ wpadd+'"}')
        cy.wait(3000)
        })
})


    it('Install Contact-7-Form Plugins + Create Contact Us Page', () => {
        cy.wait(3000)
        cy.fixture('stgcase1').then((stgcase1)  => {
            var wprul = stgcase1.URL
            cy.visit('http://admin:admin123@'+wprul)
            cy.get('a[href*="wp-login.php"]').click({force: true})
            cy.wait(2000)
            cy.get('input[name=log]').type('admin')
            cy.get('input[name=pwd]').type('abcde123456')
            cy.get('form').submit()
            cy.wait(1000)
            cy.get('#menu-plugins li:nth-child(3) > a').click({force: true})
            cy.get('.wp-filter-search').click()
            cy.get('.wp-filter-search').type('contact form 7')
            cy.wait(2000)
            cy.get('.plugin-card-contact-form-7 .install-now').click({force: true})
            cy.wait(3000)
            cy.get('.activate-now').click({force: true})
            cy.wait(1000)
            cy.get('#toplevel_page_wpcf7 li:nth-child(3) > a').click({force: true});
            cy.get('#title').type('contact us');
            cy.get('.button-primary:nth-child(2)').click()
            cy.get('#wpcf7-admin-form-element').submit()

            //get data from value then send to cypress env
            cy.get('#wpcf7-shortcode').attribute('value').then(($value) => {
              ctf7 = $value
              Cypress.env('ct7cd', ctf7)
              shrtcode = Cypress.env('ct7cd')
              cy.log('contact-7 short code: ' + shrtcode)
              //cy.writeFile('cypress/fixtures/ctf7.json', '{"Shortcode" : "'+ctf7+'"}')   //get and keep shortcode
              cy.wait(2000)


            cy.log('test dapat x code: ' + shrtcode)
            cy.log('Create Contact Us Page + Contact Form Shortocde ')
            cy.get('#menu-pages li:nth-child(3) > a').click({force: true})
            cy.wait(1000)
            cy.get('div > .editor-post-title > .wp-block > div > #post-title-0').click({force: true})
            cy.get('#post-title-0').type('Contact Us')
            cy.get('.editor-default-block-appender__content').click({force: true})
            cy.get('.rich-text').click();
            //cy.get('.current:nth-child(1)').click();
            cy.get('.editor-block-list__empty-block-inserter > .editor-inserter > .components-button > .dashicon > path').click({force: true})
            //cy.get('.editor-inserter:nth-child(4) .dashicon').click();
            cy.get('.components-panel__body:nth-child(5) .components-button').click();
            cy.get('.editor-block-types-list__list-item:nth-child(1) > .editor-block-list-item-shortcode > .editor-block-types-list__item-title').click();
            cy.get('#blocks-shortcode-input-0').click();

            var usesrtcode = shrtcode
            cy.get('#blocks-shortcode-input-0').type(usesrtcode);
            cy.wait(1000)
            cy.get('.editor-block-list__layout').click();
            cy.get('.editor-writing-flow > div:nth-child(1)').click();
            
            //publish page
            cy.get('.editor-post-publish-panel__toggle').click()
            cy.get('.editor-post-publish-button').click().click({force: true});
            cy.wait(5000)
            cy.get('.components-snackbar__content > .components-button').click({force: true}); //open  / manual check
            
            //try to complete form
            // cy.get('.focus-visible').click();
            // cy.get('.focus-visible').type('iqbal');
            // cy.get('.wpcf7-email').click();
            // cy.get('.wpcf7-email').type('iqbal@runcloud.io');
            // cy.get('.focus-visible').type('test');
            // cy.get('.wpcf7-textarea').type('test test test');
            // cy.get('.wpcf7-submit').click();
            // cy.get('.wpcf7-form').submit();

            cy.wait(5000)
            cy.wait(1000)
            
          })

           
        })

           
        //     //delete balik plugins
        //     // cy.get('#menu-plugins > .wp-has-submenu > .wp-menu-name').click({force: true});
        //     // cy.get('.current > .wp-first-item').click({force: true});
        //     // cy.get('.deactivate > a').click({force: true});
        //     // cy.get('.inactive:nth-child(2) .delete > .delete').click();


    })


    it('Back to RunCloud to Merge the Staging', () => {
        cy.visit('https://'+loginUrl+'/auth/login')
        cy.get('input[name=email]').type(username)
        cy.get('input[name=password]').type(password)
        cy.get('form').submit()
        cy.wait(5000)
        cy.get('.d-flex > .font-weight-bold').click()
        cy.wait(5000)
        cy.get(':nth-child(3) > .null > span').click()
        cy.wait(1000)
        //cy.get('.panel-heading > .pull-right > .btn').click()
        //cy.wait(1000)
        // cy.get(':nth-child(2) > a > .btn').click()

        cy.log('Merge Stagging to Production')
        cy.wait(2000)
        cy.get('tbody > :nth-child(1) > :nth-child(1) > a').click()
        cy.get(':nth-child(15) > .null > span').click()
        cy.get('#sidemenu > .nav > li:nth-child(15) > .null > span').click()
        cy.wait(1000)
        cy.get('.item:nth-child(2) .btn').click();
        cy.get('.btn-base').click();
        cy.get('form').submit();
        cy.get('.swal2-buttonswrapper').click();
        cy.get('.swal2-confirm').click();
        cy.wait(3500)

        // open merged URL (production)
        cy.log('open merged URL')
        cy.get('#sidemenu > .nav > li:nth-child(7) > .null > span').click()
        cy.get('.table > tbody > tr > td:nth-child(1) > a').click()

        cy.log('Complete! Testing!')

    })


  })
