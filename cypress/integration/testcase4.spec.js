describe('Test Case 4', function() {
    //login value change on cypress.env.json
    const username = Cypress.env('RUNCLOUD_TEST_EMAIL')  
    const password = Cypress.env('RUNCLOUD_TEST_PASSWORD')  
    const loginUrl = Cypress.env('RUNCLOUD_URL')

    it('Test Case 4 - Create WP install RunCache + Add Image', () => {
        cy.visit('https://'+loginUrl+'/auth/login')
        cy.get('input[name=email]').type(username)
        cy.get('input[name=password]').type(password)
        cy.get('form').submit()
        cy.wait(5000)
        cy.get('.d-flex > .font-weight-bold').click()
        cy.wait(5000)
        cy.get(':nth-child(3) > .null > span').click()
        cy.wait(1000)
        cy.get('.panel-heading > .pull-right > .btn').click()
        cy.wait(1000)
        cy.get(':nth-child(2) > a > .btn').click()

        //install wordpress
        cy.log('Create WordPress')
        cy.wait(3000)
        cy.get('input[name=name]').type(cy.faker.name.firstName())
        cy.get('#runcloudDomain').click()
        cy.get('input[name=siteTitle]').type('testing123')
        cy.get('input[name=adminUsername]').type('admin')
        cy.get('input[name=password]').type('abcde123456')
        cy.get('button').contains('Add Web Application').click()
        cy.wait(3000)
        cy.get('li:nth-child(7) > .null > span').click();
        //cy.get('td:nth-child(1) > a').click(); //click untk msuk wp prod
        //cy.get('.table > tbody > tr > td:nth-child(1) > a').click()
   
        //cy.visit('https://manage.runcloud.io/servers/88270/webapplications/318318/wordpress/staging?scroll=0')    // shortcut testing link

        cy.get('.table > tbody > tr > td:nth-child(1) > a').then(($a) => {
        const wpprodurl = $a.text()
        cy.writeFile('cypress/fixtures/wpprod.json', '{"URL": "'+ wpprodurl+'"}') // readURL
        cy.wait(2000) 
        
        })
    })

    it('Login WordPress and Install RunCache Plugins', () => {
        cy.wait(1000) 
        cy.fixture('wpprod').then((wpprod)  => {
            var wprul = wpprod.URL
            cy.visit('http://'+wprul)
            cy.get('a[href*="wp-login.php"]').click({force: true})
            cy.wait(2000) 
            cy.get('input[name=log]').type('admin')
            cy.get('input[name=pwd]').type('abcde123456')
            cy.get('form').submit()
            cy.wait(1000) 
            cy.get('#menu-plugins li:nth-child(3) > a').click({force: true})
            cy.get('.wp-filter-search').click()
            cy.get('.wp-filter-search').type('RunCache')
            cy.wait(3000)
            cy.get('.plugin-card-runcache-purger .install-now').click({force: true})
            cy.wait(5000)
            cy.get('.activate-now').click({force: true})
            cy.wait(1000)
           // cy.get('#toplevel_page_wpcf7 li:nth-child(3) > a').click({force: true});
            cy.get('#menu-pages li:nth-child(3) > a').click({force: true})
            cy.wait(1000)
            cy.get('div > .editor-post-title > .wp-block > div > #post-title-0').click({force: true})
            cy.get('#post-title-0').type('test image production')
            cy.get('.editor-default-block-appender__content').click({force: true})
            cy.get('.rich-text').click()
            cy.get('.components-button:nth-child(1) > .editor-block-icon > svg').click()
            cy.get('.editor-media-placeholder__url-input-container > .components-button').click()
            cy.get('.editor-media-placeholder__url-input-field').type('https://cf1.cdn.runcloud.io/img/hero/hero-banner.svg');
            cy.get('.dashicons-editor-break > path').click({force: true})
            cy.get('.editor-default-block-appender__content').click({force: true})
            cy.get('.editor-post-publish-panel__toggle').click()
            cy.get('.editor-post-publish-button').click().click({force: true});
            cy.wait(5000)

            //delete plugins
            // cy.get('#menu-plugins > .wp-has-submenu > .wp-menu-name').click({force: true});
            // cy.get('.current > .wp-first-item').click({force: true});
            // cy.get('.deactivate > a').click({force: true});
            // cy.get('.inactive:nth-child(2) .delete > .delete').click();

        })
    })
        it('Login to RunCloud to Create Stagging', () => {
            cy.visit('https://'+loginUrl+'/auth/login')
            cy.get('input[name=email]').type(username)
            cy.get('input[name=password]').type(password)
            cy.wait(3000)
            cy.get('form').submit()
            cy.wait(3000)

            //stagging
            cy.log('Create Stagging')
            cy.get('.title').click();
            cy.get('.nav-stacked > li:nth-child(3) > .null').click();
            cy.wait(3000)
            cy.get('tr > :nth-child(1) > a').click({force: true})
            cy.get(':nth-child(15) > .null > span').click()
            cy.wait(1000)
            cy.get('.panel > .panel-body > form > .form-group:nth-child(1) > .form-control').click()
            cy.get('.panel > .panel-body > form > .form-group:nth-child(1) > .form-control').click()
            cy.get('.panel > .panel-body > form > .form-group:nth-child(1) > .form-control').type(cy.faker.name.firstName() +'-stg.dev1.hadiantech.com')
            cy.get('.panel > .panel-body > form > .form-group:nth-child(5) > .form-control').click()
            cy.get('.panel > .panel-body > form > .form-group:nth-child(5) > .form-control').type('admin')
            cy.get('.panel-body > form > .form-group > .input-group > .form-control').type('admin123')
            cy.get('.panel > .panel-body > form > .form-group:nth-child(7) > .form-control').type('admin123')
            cy.get('[type="submit"]').click()
            cy.wait(7000)
            cy.get('.panel-body > :nth-child(1) > :nth-child(1) > p').then(($p) => {
            const stagcs4 = $p.text()
            cy.writeFile('cypress/fixtures/stgcase4.json', '{"URL": "'+stagcs4+'"}')
            cy.wait(2000) 
            cy.log('Complete at RC go to WP Stagging')
            })

        }) 

        it('Login to taging WordPress, Create more post + Image', () => {
            cy.wait(1000) 
            cy.fixture('stgcase4').then((stgcase4)  => {
                var wprul = stgcase4.URL
                cy.visit('http://admin:admin123@'+wprul)
                cy.get('a[href*="wp-login.php"]').click({force: true})
                cy.wait(2000) 
                cy.get('input[name=log]').type('admin')
                cy.get('input[name=pwd]').type('abcde123456')
                cy.get('form').submit()
                cy.wait(1000) 
               // create 1 page
                cy.get('#menu-pages li:nth-child(3) > a').click({force: true})
                cy.wait(1000)
                cy.get('div > .editor-post-title > .wp-block > div > #post-title-0').click({force: true})
                cy.get('#post-title-0').type('test image 1')
                cy.get('.editor-default-block-appender__content').click({force: true})
                cy.get('.rich-text').click()
                cy.get('.components-button:nth-child(1) > .editor-block-icon > svg').click()
                cy.get('.editor-media-placeholder__url-input-container > .components-button').click()
                cy.get('.editor-media-placeholder__url-input-field').type('https://cf1.cdn.runcloud.io/img/hero/hero-banner.svg');
                cy.get('.dashicons-editor-break > path').click({force: true})
                cy.get('.editor-default-block-appender__content').click({force: true})
                cy.get('.editor-post-publish-panel__toggle').click()
                cy.get('.editor-post-publish-button').click().click({force: true});
                cy.wait(5000)
                //create 2 page
                cy.get('#menu-pages li:nth-child(3) > a').click({force: true})
                cy.wait(1000)
                cy.get('div > .editor-post-title > .wp-block > div > #post-title-0').click({force: true})
                cy.get('#post-title-0').type('test image 2')
                cy.get('.editor-default-block-appender__content').click({force: true})
                cy.get('.rich-text').click()
                cy.get('.components-button:nth-child(1) > .editor-block-icon > svg').click()
                cy.get('.components-form-file-upload > .components-button')
                cy.get('.editor-writing-flow__click-redirect').click();
                //cy.get('path:nth-child(2)').click();
                cy.get('.block-editor-media-placeholder__upload-button').click({force: true});

                //cy.get('.block-editor-media-placeholder__upload-button').click({force: true}) //try upload using files
                //cy.get('.components-form-file-upload > .components-button').click({force: true})
                //cy.get('.components-form-file-upload > input').type('cypress/fixtures/pic1.jpg', {force: true});

                cy.get('.editor-media-placeholder__url-input-container > .components-button').click()
                cy.get('.editor-media-placeholder__url-input-field').type('https://cf1.cdn.runcloud.io/images/addico.png');
                cy.get('.dashicons-editor-break > path').click({force: true})
                cy.wait(1000)
                //cy.get('.editor-default-block-appender__content').click({force: true})
                cy.get('.editor-post-publish-panel__toggle').click()
                cy.get('.editor-post-publish-button').click().click({force: true});
                cy.wait(4000)
                cy.log('Complete create page + image')

            
            })
        })
            it('Merge Staging to Production', () => {
                cy.wait(3000)
                cy.visit('https://'+loginUrl+'/auth/login')
                cy.get('input[name=email]').type(username)
                cy.get('input[name=password]').type(password)
                cy.wait(3000)
                cy.get('form').submit()
                cy.wait(3000)
                //merge
                cy.log('Merge Stagging to Production')
                cy.wait(2000)
                cy.get('.d-flex > .font-weight-bold').click();
                cy.get('.nav-stacked > li:nth-child(3) > .null').click();
                cy.wait(1000)
                //cy.get('tr:nth-child(1) > td:nth-child(1) > a').click();
                cy.get('tbody > :nth-child(1) > :nth-child(1) > a').click();
                cy.get('li:nth-child(15) span').click();
                cy.get('tbody > :nth-child(1) > :nth-child(1) > a').click();
                cy.get('#sidemenu > .nav > li:nth-child(15) > .null > span').click()
                cy.wait(1000)
                cy.get('.item:nth-child(2) .btn').click();
                cy.get('.btn-base').click();
                cy.get('form').submit();
                cy.get('.swal2-buttonswrapper').click();
                cy.get('.swal2-confirm').click();
                cy.wait(3500)

                // open merged URL 
                cy.log('open merged URL')
                cy.get('#sidemenu > .nav > li:nth-child(7) > .null > span').click()
                cy.get('.table > tbody > tr > td:nth-child(1) > a').click()
        
                cy.log('Complete! Testing')

     })


})
